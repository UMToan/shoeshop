FROM maven:3.8-openjdk-17 as build

WORKDIR /build

COPY . .

RUN mvn clean install -DskipTests=true

FROM rcornu/java:8-jre-alpine

WORKDIR /app

COPY --from=build /build/target/shoeshop-0.0.1-SNAPSHOT.jar /app/shoeshop-0.0.1-SNAPSHOT.jar

RUN adduser -D shoeshop

RUN chown -R shoeshop:shoeshop /app

USER shoeshop

EXPOSE 8090

ENTRYPOINT ["java", "-jar", "/app/shoeshop-0.0.1-SNAPSHOT.jar", "--server.port=8090"]



